# Rapix Dali Zone Controller

This is a Q-SYS Plugin for the Rapix Dali Zone Controller.

> Bug reports and feature requests should be sent to Jason Foord (jf@tag.com.au).

## How do I get set up?

See [Q-SYS Online Help File - Plugins](https://q-syshelp.qsc.com/#Schematic_Library/plugins.htm)

## Properties

#### Zone Banks

Each zone bank will generate a page with 5 zone slots.

## Controls

### Zone Bank
![Zone Bank](./Screenshots/Rapix%20Plugin.JPG)

#### IP Address

The IP Address of the device.

> This is a global control that displays on every page.

#### Device Status

Displays the device's status.

> This is a global control that displays on every page.

#### Connect

Toggles the connection to the device.

> This is a global control that displays on every page.

#### Zone Slot

> ##### Number

> The Zone to control. Leave at '0' if unallocated.

>> Zone information is configured in the *Rapix Integrator* software.

> ##### Name

> The name of the zone.

> ##### Members

> Read-only control that shows the zone members.

> ##### Status

> The Status of the zone.

> ##### Level

> Control of the lighting level for the zone.

>> Can either be used in real-time, or snapshotted in Q-SYS to achieve 'preset' functionality.